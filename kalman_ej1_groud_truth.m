% Copyright (C) 2015, Raul Cabido
%parámetros iniciales
num_iters = 50;
std = 0.1;

x = -0.37727;
z = random('Normal',x,std,1,num_iters);
Q = 1e-5;

xhat = zeros (1,num_iters);
P = zeros(1,num_iters);
xhatminus = zeros (1,num_iters);
Pminus = zeros (1,num_iters);
K = zeros (1,num_iters);

R = 0.01;

xhat(1) = 0.0;
P(1) = 1;

for k =2:num_iters
   %PREDICCIÓN 
   xhatminus(k) = xhat(k-1);
   Pminus (k) = P(k-1)+Q;
   
   %CORRECCIÓN
   K(k)=Pminus(k)/(Pminus(k)+R);
   xhat(k) = xhatminus(k)+K(k)*(z(k)-xhatminus(k));
   P(k) = (1-K(k))*Pminus(k);
 
end

%Dibujado de las gráficas
figure()
iters =1:num_iters;
plot(iters,z,'k+',iters,xhat,'b-');

line(0:num_iters, x*ones(1,num_iters+1),'Color',[1,0,0]);%,label='truth value')
legend('noisy measurements','a posteri estimate', 'truth value');
xlabel('Iteration');
ylabel('Voltage');

valid_iter = 2:num_iters;
figure()
plot(valid_iter,P(valid_iter));
legend('a posteriori error estimate');
xlabel('Iteration')
ylabel('Voltage^2')
axis([2 num_iters 0 .01]);


