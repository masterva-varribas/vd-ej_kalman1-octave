%% Copyright (C) 2015 Victor Arribas
%% Author: Victor Arribas <v.arribas.urjc@gmail.com>
%% Created: 2015-02-26
%%
%% Ejercicio de introduccion a Kalman.
%% Supongamos que tomamos la medida de un voltaje (v=-0.37727)
%% ruidoso (ruido= N(0, 0.01))
%%
%% Mostrar las estimaciones para:
%% R=1, R=0.01, R=0.0001
%% 
%% Simular 50 iteraciones
%%
%% Suposiciones:
%% A=1 (modelo de transicion del sistema)
%% u=0 (no hay eventos externos)
%% R=0.01 (error de la medida: cuanto se confia en el sensor)
%% Q=10e-10 (error del sistema: cuanto se confia en A y B)

function [retval] = kalman_ej1 (input1, input2)
%% suposiciones
Q = 10e-5;
R = 0.01;
A = 1;
H = 1;


% numero de iteraciones
nIt=50;

% falseo de los datos (generados artificialmene)
% * ground truth
% * medida tomada (medida del sensor)
desajuste = 0.09;
x_k = -0.37727;
x = ones(nIt,1) * x_k;
for k=1:nIt
  %% normrnd: generacion de numeros aleatorios con distribucion normal <https://es.mathworks.com/help/stats/normrnd.html>
  v(k) = normrnd(0, R+desajuste);
  z(k) = H*x(k) + v(k);
end




%% arramcamos kalman en la etapa 'a priori' (por ejemplo),
%% luego inicializamos el estado 'a posteriori'
X(1) = 0;
P(1) = 1;

%% algoritmo
for k=2:nIt
  X_(k) = A * X(k-1) + 0;
  P_(k) = A * P(k-1) * A' + Q;
  
  
  K(k) = ( P_(k)*H' ) / ( H*P_(k)*H' + R );
  
  X(k) = X_(k) + K(k) * ( z(k) - H*X_(k) );
  P(k) = (1 - K(k)*H) * P_(k);
end


%% mostramos la evolucion del sistema
figure;
subplot(1,2,1);
title( ['R=' num2str(R) ' (R*=' num2str(R+desajuste) ')  Q=' num2str(Q) ]);
hold on;
plot(1:nIt, x, 'r');
plot(1:nIt, X, 'b');
plot(1:nIt, z, 'k+');
legend('noisy measurements','a posteri estimate', 'truth value');
xlabel('Iteration');
ylabel('Voltage');

subplot(1,2,2);
plot(1:nIt, P);
legend('a posteriori error estimate');
xlabel('Iteration')
ylabel('Voltage^2')
axis([2 nIt 0 .01]);

endfunction
